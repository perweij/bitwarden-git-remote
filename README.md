# NAME

bitwarden-dir-synk - syncronise a directory with Bitwarden

# SYNOPSIS

bitwarden-dir-synk \[options\] directory

This program syncronises a directory with Bitwarden.

Options:
   --list|-l          just list the remote dir IDs
   --name|-n          use this name as Bitwarden ID
   --help             brief help message

The Bitwarden CLI (bw) must be installed, and you should be logged in
and unlocked.

# DESCRIPTION

**bitwarden-dir-synk** ...

# AUTHOR

Written by Per Weijnitz.

# REPORTING BUGS

...
