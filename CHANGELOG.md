## 0.3.5 (2023-03-06)

No changes.

## 0.0.5 (2023-03-06)

No changes.

## 0.0.4 (2023-03-05)

No changes.

## 0.0.3 (2023-03-03)

### feature (1 change)

- [Make nothing, just learn Changelog generation](perweij/bitwarden-git-remote@e44f1acce4050c53ae6a5ab8d3f9b8edbc5b0402)

## 0.0.2 (2023-03-03)

### feature (1 change)

- [Add tagging](perweij/bitwarden-git-remote@8591158ba2aee3a325eb1b06318c873c4a4279ed)

## 0.0.1 (2023-03-03)

No changes.
